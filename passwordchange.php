<?php
    require_once 'core/init.php';
    $user = new User();
    if (!$user->isLoggedIn()) {
        Redirect::to('index.php');
    }
    if (Input::exists()) {
        if (Token::check(Input::get('token'))) {
            $validate = new Validate();
            $validation = $validate->check($_POST, array(
                'current_password' => array(
                    'required' => true,
                    'min' => 2
                ),
                'new_password' => array(
                    'required' => true,
                    'min' => 2
                ),
                'new_password_again' => array(
                    'required' => true,
                    'min' => 2,
                    'matches' => 'new_password'
                )
            ));
            if ($validation->passed()) {
                try {
                    if (Hash::make(Input::get('current_password'), $user->data()->salt) !== $user->data()->password) {
                        echo 'Your current password is wrong!';
                    } else {
                        $salt = '';
                        $user->update(array(
                            'password' => Hash::make(Input::get('new_password'), $salt),
                            'salt' => $salt
                        ));
                        Session::flash('success', 'Your password has been changed!');
                        Redirect::to('index.php');
                    }
                } catch(Exception $e) {
                    die($e->getMessage());
                }
            } else {
                foreach ($validation->errors() as $error) {
                    echo $error, '<br>';
                }
            }
        } else {
            echo 'Token dint pass!';
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/main.css?v=<?php echo time(); ?>">
    <link rel="stylesheet" href="/css/whitebox.css?v=<?php echo time(); ?>">
    <title>MegaBit</title>
</head>
<body>
    <div class="loginback">
        <div>
            <h2 class="login-text">UPDATE INFO</h2>
            <hr class="full">
        </div>
            <form action="" method="post">
                <div class="relative">
                    <input type="password" name="current_password" autocomplete="off">
                    <label for="current_password">Current Password</label>
                    <hr class="login_underline">
                </div>
                <div class="relative">
                    <input type="password" name="new_password" autocomplete="off">
                    <label for="email">New password</label>
                    <hr class="login_underline">
                </div>
                <div class="relative">
                    <input type="password" name="new_password_again" autocomplete="off">
                    <label for="email">New password again</label>
                    <hr class="login_underline">
                </div>
                <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                <input class="button-yellow" type="submit" name="update" value="UPDATE">
            </form>
            <a class="button-yellow" href="index.php">BACK</a>
        </div>
    </div>
<script src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
</body>
</html>