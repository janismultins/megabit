<?php
    require_once 'core/init.php';
    $user = new User();
    if (!$user->isLoggedIn()) {
        Redirect::to('index.php');
    }
    if (Input::exists()) {
        if (Token::check(Input::get('token'))) {
            $validate = new Validate();
            $validation = $validate->check($_POST, array(
                'username' => array(
                    'required' => true,
                    'min' => 2,
                    'max' => 20,
                ),
                'email' => array(
                    'required' => true,
                    'min' => 2,
                    'max' => 50,
                )
            ));
            if ($validation->passed()) {
                try {
                    $user->update(array(
                        'username' => Input::get('username'),
                        'email' => Input::get('email'),
                    ));
                    Session::flash('success', 'Your profile info has been updated!');
                    Redirect::to('index.php');
                } catch(Exception $e) {
                    die($e->getMessage());
                }
            } else {
                foreach ($validation->errors() as $error) {
                    echo $error, '<br>';
                }
            }
        } else {
            echo 'Token dint pass!';
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/main.css?v=<?php echo time(); ?>">
    <link rel="stylesheet" href="/css/whitebox.css?v=<?php echo time(); ?>">
    <title>MegaBit</title>
</head>
<body>
    <div class="loginback">
        <div>
            <h2 class="login-text">UPDATE INFO</h2>
            <hr class="full">
        </div>
            <form action="" method="post">
                <div class="relative">
                    <input type="text" name="username" autocomplete="off" value="<?php echo escape($user->data()->username) ?>">
                    <label for="username">Name</label>
                    <hr class="login_underline">
                </div>
                <div class="relative">
                    <input type="text" name="email" autocomplete="off" value="<?php echo escape($user->data()->email) ?>">
                    <label for="email">Email</label>
                    <hr class="login_underline">
                </div>
                <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                <input class="button-yellow" type="submit" name="update" value="UPDATE">
            </form>
            <a class="button-yellow" href="index.php">BACK</a>
        </div>
    </div>
<script src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
</body>
</html>