<?php
    require_once 'core/init.php';
    if (Input::exists(isset($_POST['register']))) {
        if (Token::check(Input::get('token'))) {      
            $validate = new Validate();
            $validation = $validate->check($_POST, array(
                'username' => array(
                    'required' => true,
                    'min' => 2,
                    'max' => 20,
                    'unique' => 'users'
                ),
                'email' => array(
                    'required' => true,
                    'min' => 2,
                    'max' => 50,
                    'unique' => 'users'
                ),
                'password' => array(
                    'required' => true,
                    'min' => 6,
                )
            ));
            if ($validation->passed()) {
                $user = new User();
                $salt = '';
                try {
                    $user->create(array(
                        'username' => Input::get('username'),
                        'email' => Input::get('email'),
                        'password' => Hash::make(Input::get('password'), $salt),
                        'salt' => $salt,
                        'joined' => date('Y-m-d H:i:s')
                    ));
                    echo 'User successfully registred!';
                } catch(Exception $e) {
                    die($e->getMessage());
                }
            } else {
                foreach ($validation->errors() as $error) {
                    echo $error, '<br>';
                }
            }
        } else {
            echo 'Register Token Didnt Pass!' . '<br>';
        }
    }
    if (Input::exists(isset($_POST['login']))) {
        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            'email' => array('required' => true,),
            'password' => array('required' => true)
        ));
        if ($validation->passed()) {
            $user = new User();
            $login = $user->login(Input::get('email'), Input::get('password'));

            if ($login) {
                Redirect::to('loggedin.php');
            } else {
                echo '<p> Logging in failed! <p>';
            }
        } else {
            foreach ($validation->errors() as $error) {
                echo $error, '<br>';
            }
        }
    }   
    $user = new User();
    if ($user->isLoggedIn()) {
        Redirect::to('loggedin.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">  
    <link rel="stylesheet" href="/css/whitebox.css?v=<?php echo time(); ?>">
    <link rel="stylesheet" href="/css/main.css?v=<?php echo time(); ?>">
    <title>MegaBit</title>
</head>
<body>
    <div class="mainbackground">
        <div class="signuptext">
            <h2>Don�t have an account?</h2>
            <hr class="solid">
            <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h3>
            <button class="signup" id="signup">SIGN UP</button>
        </div>
        <div class="logintext">
            <h2>Have an account?</h2>
            <hr class="solid">
            <h3>Lorem ipsum dolor sit amet consectetur adipisicing elit.</h3>
            <button class="signup" id="login">LOGIN</button>
        </div>
        <div class="whitebox right" id="whitebox">
            <!-- LOGIN -->
            <div class="loginbox">
                <div class="relative">
                    <h2 class="login-text">Login</h2>
                    <img class="logo" src="/images/logo.png" alt="">
                </div>
                <hr class="solidd">
                <form form action="" method="post">
                    <div class="field relative">
                        <input type="text" name="email" autocomplete="off">
                        <label class="demo-label" for="email">Email<span>*</span></label>
                        <img class="input-icon" src="/images/icons/ic_mail.png" alt="">
                        <hr class="login_underline">
                    </div>
                    <div class="field relative">
                        <input type="password" name="password">
                        <label for="email">Password<span>*</span></label>
                        <img class="input-icon" src="/images/icons/ic_lock.png" alt="">
                        <hr class="login_underline">
                    </div>
                    <input class="button-yellow" type="submit" name="login" value="LOGIN">
                </form>
            </div>
            <!-- LOGIN END -->
            <!-- SIGN UP -->
            <div class="signupbox">
                <div class="relative">
                    <h2 class="login-text">SIGN UP</h2>
                    <img class="logo" src="/images/logo.png" alt="">
                </div>
                <hr class="solidd">
                <form action="" method="post">
                    <div class="field relative">
                        <input type="text" name="username" id="username" value="<?php echo escape(Input::get('username')); ?>" autocomplete="off">
                        <label class="magic" for="username">Name<span>*</span></label>
                        <img class="input-icon" src="/images/icons/ic_user.png" alt="">
                        <hr class="login_underline">
                    </div>
                    <div class="field relative">
                        <input type="text" name="email" id="email" value="<?php echo escape(Input::get('email')); ?>" autocomplete="off">
                        <label for="email">Email<span>*</span></label>
                        <img class="input-icon" src="/images/icons/ic_mail.png" alt="">
                        <hr class="login_underline">
                    </div>
                    <div class="field relative">
                        <input type="password" name="password" id="password" value="" autocomplete="off">
                        <label for="password">Password<span>*</span></label>
                        <img class="input-icon" src="/images/icons/ic_lock.png" alt="">
                        <hr class="login_underline">
                    </div>
                    <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                    <input class="button-yellow" type="submit" name="register" value="SIGN UP">
                </form>
            </div>
            <!-- SIGN UP END -->
        </div>
    </div>

<script src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
</body>
</html>