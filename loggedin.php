<?php
    require_once 'core/init.php';
    $user = new User();
    if (!$user->isLoggedIn()) {
        Redirect::to('index.php');
    }
    echo Session::flash('success');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/main.css?v=<?php echo time(); ?>">
    <link rel="stylesheet" href="/css/whitebox.css?v=<?php echo time(); ?>">
    <title>MegaBit</title>
</head>
<body> 
    <div class="loginback">
        <div>
            <h2>Hello <?php echo escape($user->data()->username); ?></h2>
            <hr class="full">
            <h3 class="loggedin">Username: <?php echo escape($user->data()->username); ?></h3>
            <h3 class="loggedin">Email: <?php echo escape($user->data()->email); ?></h3>
            <h3 class="loggedin">Joined: <?php echo escape($user->data()->joined); ?></h3>
            <a class="button-yellow" href="updateinfo.php">UPDATE INFO</a>
            <br>
            <a class="button-yellow" href="passwordchange.php">UPDATE PASSWORD</a>
            <br>
            <a class="button-yellow" href="logout.php">LOGOUT</a>
        </div>
    </div>
<script src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
</body>
</html>