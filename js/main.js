$(document).ready(function () {
    $("#signup").click(function (){
        $("#whitebox").animate({left: '20px'}, 900, function(){
            $("#whitebox").removeClass('right');
            $("#whitebox").addClass('left');
            $("#whitebox").removeAttr('style');
        });
        $(".loginbox").fadeOut("fast", function(){
            $(".signupbox").fadeIn();
        });
    });
    $("#login").click(function (){
        $("#whitebox").animate({right: '20px'}, 900, function(){
            $("#whitebox").removeAttr('style');
        });
        $("#whitebox").addClass('right');
        $("#whitebox").removeClass('left');
        $(".signupbox").fadeOut(function(){
            $(".loginbox").fadeIn();
        });
    });
});